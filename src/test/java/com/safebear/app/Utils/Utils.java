package com.safebear.app.Utils;

import org.openqa.selenium.WebDriver;

/**
 * Created by cca_student on 24/10/2017.
 */
public class Utils {

    WebDriver driver;
    String url;

    public boolean navigateToWebsite(WebDriver driver) {
        this.driver = driver;
        url = "http://automate.safebear.co.uk/";
        driver.get(url);
        return driver.getTitle().startsWith("Welcome");
    }
}

