package com.safebear.app;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by cca_student on 24/10/2017.
 */
public class Test01_login extends BaseTest {
    @Test
    public void testLogin () {
    //Step 1 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());
        //Step 2 click on the Login link and the Login Page loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));
        //Step 3 Login with vaild credentials
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));
    }

}