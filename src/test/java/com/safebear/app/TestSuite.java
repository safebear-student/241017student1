package com.safebear.app;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by cca_student on 24/10/2017.
 */
@RunWith(Suite.class)
    @Suite.SuiteClasses({
            Test01_login.class,
    })
public class TestSuite {
}
