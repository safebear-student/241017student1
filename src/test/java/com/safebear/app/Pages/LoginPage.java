package com.safebear.app.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by cca_student on 24/10/2017.
 */
public class LoginPage {

    WebDriver driver;

    @FindBy(id = "myid")
    WebElement username_field;
    @FindBy(id="mypass")
    WebElement password_field;

    public LoginPage (WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public boolean checkCorrectPage(){
        return driver. getTitle().startsWith("Sign In");
    }

    public boolean login (UserPage userPage, String Username, String password){
        this.username_field.sendKeys(Username);
        this.password_field.sendKeys(password);
        this.password_field.submit();
        return userPage.checkCorrectPage();
    }
}
