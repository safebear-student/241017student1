package com.safebear.app.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by cca_student on 24/10/2017.
 */
public class UserPage {
    WebDriver driver;

    public UserPage (WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public boolean checkCorrectPage(){
        return driver. getTitle().startsWith("Logged In");
    }
}
