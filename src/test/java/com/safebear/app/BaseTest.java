package com.safebear.app;

import com.safebear.app.Pages.LoginPage;
import com.safebear.app.Pages.UserPage;
import com.safebear.app.Pages.WelcomePage;
import com.safebear.app.Utils.Utils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.DriverCommand;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by cca_student on 24/10/2017.
 */
public class BaseTest {
    WebDriver driver;
    Utils utility;
    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage userPage;

    @Before
    public void setUp() throws MalformedURLException {

//        driver = new ChromeDriver();
        DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit(); driver = new
                RemoteWebDriver( new URL("http://127.0.0.1:4444/wd/hub"),capabilities);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        utility = new Utils();
        welcomePage = new WelcomePage(driver);
        loginPage = new LoginPage(driver);
        userPage = new UserPage(driver);
        assertTrue(utility.navigateToWebsite(driver));
    }

    @After
    public void tearDown() {

        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }
}
